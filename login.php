<?php
//panggil koneksi
include 'config/koneksi.php';
session_start();
if (isset($_POST['login'])) {
   // simpan nilai inoutan pd variabel
   $user =  $_POST['username'];
   $pass =  md5($_POST['password']);
   // cek pd query nilai input dan nilai pd tabel
   $sql =  mysqli_query($koneksi, "SELECT * FROM login WHERE Username = '$user' AND Password = '$pass'");
   $data = mysqli_fetch_array($sql);
   // cek jumlah data
   if (mysqli_num_rows($sql) > 0) {
      // simpan nilai pd sesi
      $_SESSION['user'] = $user;
      $_SESSION['status'] = "login";
      $_SESSION['nama'] = $data['Name'];
      // setelah aray sesi disimpan, arahkan ke index.php
      echo '<script>
                setTimeout(function() {
                    swal({
                        title: "SUCCESS",
                        text: "Berhasil Login!",
                        type: "success"
                    }, function() {
                        window.location = "index.php";
                    });
                }, 500);
            </script>';
   } else {
      header("location:login.php?status=gagal");
   }
}

?>



<!doctype html>
<html lang="en">

<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link rel="icon" href="img/buku.png" type="image/ico">

   <!-- tambahan dari gitlab: michalsnik/aos -->
   <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


   <style>
      * {
         padding: 0;
         margin: 0;
         box-sizing: border-box;
      }

      body {
         width: 100%;
         height: 667px;
         display: flex;
         flex-wrap: wrap;
         justify-content: center;
         align-items: center;
         background: #007bff;
         background: linear-gradient(to right, #0062E6, #33AEFF);
      }

      .box {
         width: 100%;
         display: flex;
         flex-wrap: wrap;
         justify-content: space-around;
         background-color: white;
         border-radius: 8px;
         padding: 20px 20px;
      }

      .contentForm {
         margin: 8%;
      }

      .form-control {
         border-color: blue;
         border-style: solid;
         border-width: 0 0 1px 0;
      }

      .form-control:focus {
         border-width: 0 0 3px 0;
         color: black;
         box-shadow: none;
         transition: all 0.1s ease-out;
      }

      .btn {
         border-radius: 20px;
      }

      .btn:hover {
         background: white;
         border: 1px solid;
         color: black;
      }
   </style>

   <title>Login | R6C Library</title>
</head>

<body>
   <div class="container mt-3 mb-3">
      <div class="box">
         <div class="row contentForm">
            <div class="col-sm-12 col-md-6 col-lg-6">
               <!-- tambahan dari gitlab: michalsnik/aos -->
               <img data-aos="fade-in" data-aos-duration="1000" data-aos-easing="ease-in-out" src="img/login.jpg" alt="" class="img-fluid">
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
               <h4 class="text-center mb-5">R6C LIBRARY📘
                  <hr class="border-primary"> Login System
               </h4>
               <?php
               if (isset($_GET['status'])) {
                  if ($_GET['status'] == 'logindulu') {
                     echo "<div class='alert alert-warning'>Maaf! Silahkan Login Terlebih Dahulu</div>";
                  } elseif ($_GET['status'] == 'gagal') {
                     echo "<div class='alert alert-danger'>Username / Password Tidak Sesuai!</div>";
                  } elseif ($_GET['status'] == 'logout') {
                     echo "<div class='alert alert-success'> Berhasil Logout</div>";
                  }
               }
               ?>

               <form class="mt-3" method="POST">
                  <div class="form-group mb-3">
                     <label for="exampleInputEmail1">Username </label>
                     <input type="text" class="form-control" id="exampleInputEmail1" name="username" aria-describedby="emailHelp" placeholder="example.com" required>
                  </div>
                  <div class="form-group mb-3">
                     <label for="exampleInputPassword1">Password</label>
                     <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="**********">
                  </div>

                  <button name="login" type="submit" class="btn btn-primary w-100  mb-3">Login</button>

                  <p class="mt-4">Belum Punya Akun? <a href="registrasi.php">Buat Akun Baru</a></p>

               </form>
            </div>

         </div>
      </div>
   </div>



   <!-- Bootstrap core JavaScript-->
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- Core plugin JavaScript-->
   <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

   <!-- Custom scripts for all pages-->
   <script src="js/sb-admin-2.min.js"></script>
   <!-- s -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

   <!-- tambahan dari gitlab: michalsnik/aos -->
   <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
   <script>
      AOS.init();
   </script>
</body>

</html>