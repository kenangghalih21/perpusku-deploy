<?php
//panggil koneksi
include 'config/koneksi.php';
function registrasi($data)
{ //menerima inputan data yg dikirim dari $post, ditangkap kesini
   global $koneksi; //ambil koneksi paling atas hlm ini

   $username = strtolower(stripslashes($data["username"])); //ambil data dari $_post, diambil olh var $data
   $nama = strtolower(stripslashes($data["nama"]));
   $password = mysqli_real_escape_string($koneksi, $data["password"]); //ada 2 parameter
   $password2 = mysqli_real_escape_string($koneksi, $data["password2"]);

   // cek username sudah ada atau belum
   $result = mysqli_query($koneksi, "SELECT Username FROM login WHERE Username = '$username'");
   if (mysqli_fetch_assoc($result)) {
      echo "<script>
			alert('Username Telah Terdaftar!');
			</script>";
      return false;   //berhentikan functionnya, supaya insert gagal dan yg bawah tidak dijalankan
   }

   // cek konfirmasi password
   if ($password !== $password2) {
      echo "<script>
				alert('Password Tidak Sesuai!');
			</script>";

      return false; //berhentikan function, supaya masuk ke else di registrasi
   }
   // enkripsi password
   $password = md5($password);

   // tambahkan userbaru ke database
   mysqli_query($koneksi, "INSERT INTO login VALUES('', '$username', '$password', '$nama')");
   return mysqli_affected_rows($koneksi);
}

if (isset($_POST["register"])) {

   if (registrasi($_POST) > 0) {
      echo "<script>
            location.href='login.php';
				alert ('User Baru Berhasil Ditambahkan!');
		  	  </script>";
   } else {
      echo mysqli_error($koneksi);
   }
}


?>



<!doctype html>
<html lang="en">

<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link rel="icon" href="img/buku.png" type="image/ico">

   <!-- tambahan dari gitlab: michalsnik/aos -->
   <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
   <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
   <link href="css/sb-admin-2.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


   <style>
      * {
         padding: 0;
         margin: 0;
         box-sizing: border-box;
      }

      body {
         width: 100%;
         height: 667px;
         display: flex;
         flex-wrap: wrap;
         justify-content: center;
         align-items: center;
         background: #007bff;
         background: linear-gradient(to right, #0062E6, #33AEFF);
      }

      .box {
         width: 100%;
         display: flex;
         flex-wrap: wrap;
         justify-content: space-around;
         background-color: white;
         border-radius: 8px;
         padding: 20px 20px;
      }

      .contentForm {
         margin: 8%;
      }

      .form-control {
         border-color: blue;
         border-style: solid;
         border-width: 0 0 1px 0;
      }

      .form-control:focus {
         border-width: 0 0 3px 0;
         color: black;
         box-shadow: none;
         transition: all 0.1s ease-out;
      }

      .btn {
         border-radius: 20px;
      }

      .btn:hover {
         background: white;
         border: 1px solid;
         color: black;
      }
      #hitam{
          color: black;
          font-weight: bold;
      }
   </style>

   <title>Registrasi | R6C Library</title>
</head>

<body>
   <div class="container mt-3 mb-3">
      <div class="box">
         <div class="row contentForm">

            <div class="col-sm-12 col-md-6 col-lg-6">
               <h4 class="text-center mb-5" id="hitam">R6C LIBRARY📘
                  <hr class="border-primary"> Form Registrasi
               </h4>

               <form class="mt-3" method="POST">
                  <div class="form-group mb-3">
                     <input type="text" class="form-control" id="username" name="username" aria-describedby="" placeholder="Username" required>
                  </div>
                  <div class="form-group mb-3">
                     <input type="text" class="form-control" id="nama" name="nama" aria-describedby="" placeholder="Nama" required>
                  </div>

                  <div class="form-group mb-3">
                     <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                  </div>

                  <div class="form-group mb-3">
                     <input type="password" class="form-control" name="password2" id="password2" placeholder="Confirm Password" required>
                  </div>

                  <button name="register" type="submit" class="btn btn-primary w-100  mb-3">Registrasi</button>

                  <p class="mt-4">Sudah Punya Akun? <a href="login.php">Kembali ke Halaman Login</a></p>

               </form>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
               <!-- tambahan dari gitlab: michalsnik/aos -->
               <img data-aos="fade-in" data-aos-duration="1000" data-aos-easing="ease-in-out" src="img/register.jpg" alt="" class="img-fluid">
            </div>

         </div>
      </div>
   </div>



   <!-- Bootstrap core JavaScript-->
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   <!-- Core plugin JavaScript-->
   <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

   <!-- Custom scripts for all pages-->
   <script src="js/sb-admin-2.min.js"></script>
   <!-- s -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

   <!-- tambahan dari gitlab: michalsnik/aos -->
   <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
   <script>
      AOS.init();
   </script>
</body>

</html>