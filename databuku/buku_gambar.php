<?php

include 'config/koneksi.php';
if (isset($_POST['upload'])) {
   $ekst_oke = array('jpg', 'jpeg', 'png'); //ekstensi file yang diperbolehkan
   $namafile = $_FILES['gambar']['name']; //mengambil nama file melalui inputan

   $exp = explode('.', $namafile); //memisahkan nama file dan ekstensi
   $ekstensi = strtolower(end($exp)); //mengambil ekstensi penamaan terakhir file
   $ukuran = $_FILES['gambar']['size']; //mengambil ukuran file
   $file_tmp = $_FILES['gambar']['tmp_name'];

   $kd = $_POST['a'];

   if (in_array($ekstensi, $ekst_oke) === TRUE) { //jika ekstensi sesuai dan diperbolehkan
      if ($ukuran <= 3044070) { //jika ukuran lbh kecil/sama dgn 1 MB
         //maka pindahkan file ke folder tujuan
         move_uploaded_file($file_tmp, 'upload_file/' . $namafile);
         $sql = mysqli_query($koneksi, "UPDATE buku SET Gambar = '$namafile' WHERE KodeBuku = '$kd'");
         if ($sql) {
            echo "<script>alert('Gambar Berhasil Diupload!');location.href='index.php?page=view-buku';</script>";
         }
      } else {
         echo "<script>alert('Ukuran File Terlalu Besar!');location.href='index.php?page=view-buku';</script>";
      }
   } else {
      echo "<script>alert('Ekstensi File Tidak Diizinkan!');location.href='index.php?page=view-buku';</script>";
   }
}

$kd = $_GET['kd'];
$tampil = mysqli_query($koneksi, "SELECT * FROM buku WHERE KodeBuku = '$kd'");
while ($data = mysqli_fetch_array($tampil)) { ?>

   <div class="row">
      <div class="col-md-12">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
               <h6 class="m-0 font-weight-bold text-primary">Ubah Data Buku</h6>
               <a href="index.php?page=view-buku" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-eye fa-sm text-white-50"></i> Tampil Data</a>
            </div>

            <form action="" method="POST" enctype="multipart/form-data">
               <div class="card-body col-md-6">
                  <div class="form-group">
                     <label>Kode Buku</label><br>
                     <h4><?= $data['KodeBuku'] ?></h4>
                     <input type="hidden" name="a" class="form-control" value="<?= $data['KodeBuku'] ?>" placeholder="Enter Kode Buku" required>
                  </div>
                  <div class="form-group">
                     <label>Judul Buku</label>
                     <h5><?= $data['Judul'] ?></h5>
                  </div>
                  <div class="form-group">
                     <label>Upload Gambar</label>
                     <input type="file" name="gambar" class="form-control">
                  </div>
               </div>
               <div class="card-footer">
                  <button type="submit" name="upload" class="btn btn-primary"> <i class="fa fa-save"></i> Upload Data</button>
               </div>
            </form>

         </div>
      </div>
   </div>

<?php } ?>