<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>pdf report</title>
   <link href="../css/sb-admin-2.min.css" rel="stylesheet">
   <link rel="icon" href="../img/buku.png" type="image/ico">

</head>

<body onload="window.print()">
   <h3>Laporan Data Buku</h3>
   <hr>
   <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead>
         <tr>
            <td>No.</td>
            <td>Kode Buku</td>
            <td>Judul</td>
            <td>Kategori</td>
            <td>Thn</td>
            <td>Penerbit</td>
            <td>Pengarang</td>
         </tr>
      </thead>
      <tbody>
         <?php
         include '../config/koneksi.php';
         $no = 1;
         $sql = mysqli_query($koneksi, "SELECT*FROM buku LEFT JOIN kategori ON buku.Kategori = kategori.IDKategori");
         while ($data = mysqli_fetch_array($sql)) { ?>
            <tr>
               <td><?= $no++ ?></td>
               <td><?= $data['KodeBuku'] ?></a></td>
               <td><?= $data['Judul'] ?></td>
               <td><?= $data['NmKategori'] ?></td>
               <td><?= $data['Thn'] ?></td>
               <td><?= $data['Penerbit'] ?></td>
               <td><?= $data['Pengarang'] ?></td>
            </tr>
         <?php } ?>
      </tbody>
   </table>

</body>

</html>