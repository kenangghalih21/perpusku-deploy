<div class="row">
   <div class="col-md-12">
      <div class="card shadow mb-4">
         <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
            <h6 class="m-0 font-weight-bold text-primary">List Data Buku</h6>
            <div class="btn btn-group">
               <a href="index.php?page=in-buku" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data</a>
               <a href="databuku/buku_print.php" onclick="NewWindow(this.href,'name','840','1070','yes');return false" class="d-sm-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-print fa-sm text-white-50"></i> Cetak Data</a>
            </div>

         </div>
         <div class="card-body">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               <thead>
                  <tr>
                     <td>No.</td>
                     <td>Kode Buku</td>
                     <td>Judul</td>
                     <td>Kategori</td>
                     <td>Thn</td>
                     <td>Gambar</td>
                     <td>Aksi</td>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  include 'config/koneksi.php';
                  $no = 1;
                  $sql = mysqli_query($koneksi, "SELECT*FROM buku LEFT JOIN kategori ON buku.Kategori = kategori.IDKategori ORDER BY KodeBuku ASC");
                  while ($data = mysqli_fetch_array($sql)) { ?>
                     <tr>
                        <td><?= $no++ ?></td>
                        <td> <a href="index.php?page=detail-buku&kd=<?= $data['KodeBuku'] ?>" class="btn btn-sm btn-primary shadow-sm"><?= $data['KodeBuku'] ?></a></td>
                        <td><?= $data['Judul'] ?></td>
                        <td><?= $data['NmKategori'] ?></td>
                        <td><?= $data['Thn'] ?></td>
                        <td><?php if ($data['Gambar'] == 'default.jpg') { ?>
                              <img src="upload_file/default.jpg" width="100px">
                           <?php } else { ?>
                              <img src="upload_file/<?= $data['Gambar'] ?>" width="100px">
                           <?php } ?>
                        </td>
                        <td>
                           <div class="btn-group">
                              <a href="index.php?page=add-gambar&kd=<?= $data['KodeBuku'] ?>" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-image"></i></a>
                              <a href="index.php?page=up-buku&kd=<?= $data['KodeBuku'] ?>" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-edit"></i></a>
                              <a href="databuku/buku_del.php?kd=<?= $data['KodeBuku'] ?>" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-trash"></i></a>
                           </div>
                        </td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>