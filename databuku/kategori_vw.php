<div class="row">
   <div class="col-md-12">
      <div class="card shadow mb-4">
         <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
            <h6 class="m-0 font-weight-bold text-primary">List Kategori Buku</h6>

         </div>
         <div class="card-body">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               <thead>
                  <tr>
                     <td>No.</td>
                     <td>Kategori</td>
                     <td>Judul</td>
                     <td>Kode Buku</td>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  include 'config/koneksi.php';
                  $no = 1;
                  $sql = mysqli_query($koneksi, "SELECT*FROM buku LEFT JOIN kategori ON buku.Kategori = kategori.IDKategori ORDER BY kategori");
                  while ($data = mysqli_fetch_array($sql)) { ?>
                     <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data['NmKategori'] ?></td>
                        <td><?= $data['Judul'] ?></td>
                        <td> <a href="index.php?page=detail-buku&kd=<?= $data['KodeBuku'] ?>" class="btn btn-sm btn-primary shadow-sm"><?= $data['KodeBuku'] ?></a></td>
                        </td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>