<style>
   .img-fluid {
      border-radius: 4px;
      background: #fff;
      box-shadow: 0 6px 10px rgba(0, 0, 0, .08), 0 0 6px rgba(0, 0, 0, .05);
      transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12);
      cursor: pointer;
   }

   .img-fluid:hover {
      transform: scale(1.05);
      box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06);
   }

</style>
<div class="text-center mt-2 mb-5">
   <img src="img/g1.jpg" class="p-2 img-fluid mt-2 mb-2" alt="..." width="700px">
   <img src="img/g2.jpg" class="p-2 img-fluid mt-2 mb-2" alt="..." width="700px">
   <img src="img/g3.jpg" class="p-2 img-fluid mt-2 mb-2" alt="..." width="700px">
</div>