<style>
    .gg{
      border-radius: 4px;
      background: #fff;
      box-shadow: 0 6px 10px rgba(0, 0, 0, .08), 0 0 6px rgba(0, 0, 0, .05);
      transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12);
      cursor: pointer;
   }
   .gg:hover {
      transform: scale(1.05);
      box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06);
   }
    }
</style>



<div class="row">
   <div class="col-md-9">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
         </ol>
         <div class="carousel-inner">
            <div class="carousel-item active">
               <img class="d-block w-100" src="img/cover1.jpg" alt="First slide">
               <div class="carousel-caption d-inline p-3 bg-primary text-white">
                  <h3>WELCOME <?= $_SESSION['nama'] ?></h3>
                  <h5>Selamat Datang Di Aplikasi R6C Library!</h5>
               </div>
            </div>
            <div class="carousel-item">
               <img class="d-block w-100" src="img/cover2.jpg" alt="Second slide">
               <div class="carousel-caption d-inline p-2 bg-white text-dark">
                  <h5>Aplikasi R6C Library merupakan aplikasi yang diciptakan oleh tim developer R6C untuk mengelola data buku yang ada di perpustakaan Unindra!</h5>
               </div>
            </div>

         </div>
         <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </a>
      </div>
   </div>
      <div class="col-md-3 border-left-primary">
      <div class="card-body">
         <h3 class="text-primary">R6C LIBRARY📘</h3>
         <p>Responsive Web Application based on PHP, MySql and made by CSS Framework: Bootstrap with sbadmin-2. <br> Not only use template but we're design and mix with our own ideas and works. <br><br> Sincerely created <a href="index.php?page=profil"> by us</a>.</p>
        <button type="button" onclick="location.href='index.php?page=view-buku'" class="d-sm-inline-block btn btn-sm btn-primary shadow-sm"> <i class="fas fa-eye fa-sm text-white-50"></i> Check Our Book-list </button>
         <hr class="border-primary">
         <h3 class="text-primary">Our Features:</h3>
         <ul>
            <li>Responsive Web</li>
            <li>Login Application</li>
            <li>CRUD</li>
            <li>Upload Image</li>
            <li>PDF Reporting</li>
            <li>Ajax - javascript</li>
            <li>SweetAlert</li>
         </ul>
         <a href="https://gitlab.com/kenangghalih21/r6c-library" target="_blank">Our commit on gitlab</a>
      </div>
   </div>

</div>
<hr class="border-dark mt-5 mb-5">
<div class="row mb-5">
   <section id="tools" class="offset container">
      <div class="post-heading text-center">
         <h3 class="text-dark display-5 bold">Tools</h3>
         <hr class="w-50 mx-auto pb-3 border-primary">
      </div>
      <div class="d-flex flex-wrap justify-content-center pt-1 mb-2" id="tools_logo">
         <img src="img/logo-html5.png" class="gg border bg-light rounded mx-1" width="70">
         <img src="img/logo-css3.png" class="gg border bg-light border-rounded mx-1" width="70">
          <img src="img/logo-bootstrap.png" class="gg border bg-light rounded mx-1" width="85">
         <img src="img/php.svg" class="gg border bg-light rounded mx-1" width="70">
         <img src="img/logo-javascript.png" class="gg border bg-light rounded mx-1" width="70">
         <img src="img/logo-git.png" class="gg border bg-light rounded mx-1" width="70">
         <img src="img/logo-vscode.png" class="gg border bg-light rounded mx-1" width="70">
         <!--<img src="img/logo-gitlab.png" class="gg border bg-light rounded mx-1" width="70">-->



      </div>
   </section>

</div>